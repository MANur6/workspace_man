package Vista;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import java.awt.CardLayout;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.ScrollPane;
import javax.swing.JTable;
import java.awt.Scrollbar;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;

public class Vista_Principal extends JFrame {

	private JPanel contentPane;
	private JButton btnTurnos;
	private JTable tablaPrincipal;
	private JButton btnReportes;
	private JButton btnConfiguracion;
	private JButton btnGestion;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Vista_Principal frame = new Vista_Principal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Vista_Principal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 729, 377);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new CardLayout(0, 0));
		
		JPanel panelPrincipal = new JPanel();
		contentPane.add(panelPrincipal, "panelPrincipal");
		panelPrincipal.setLayout(null);
		
		JPanel panelBotonera = new JPanel();
		panelBotonera.setBackground(Color.LIGHT_GRAY);
		panelBotonera.setBounds(6, 6, 164, 333);
		panelPrincipal.add(panelBotonera);
		panelBotonera.setLayout(null);
		
		btnTurnos = new JButton("TURNOS");
		btnTurnos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnTurnos.setBounds(6, 131, 152, 41);
		panelBotonera.add(btnTurnos);
		
		btnReportes = new JButton("REPORTES");
		btnReportes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnReportes.setBounds(6, 184, 152, 41);
		panelBotonera.add(btnReportes);
		
		btnConfiguracion = new JButton("USUARIOS");
		btnConfiguracion.setBounds(6, 237, 152, 41);
		panelBotonera.add(btnConfiguracion);
		
		btnGestion = new JButton("GESTION");
		btnGestion.setBounds(6, 286, 152, 41);
		panelBotonera.add(btnGestion);
		
		JButton btnNewButton = new JButton("imagen");
		btnNewButton.setBounds(6, 6, 152, 113);
		panelBotonera.add(btnNewButton);
		
		JPanel panel_Card = new JPanel();
		panel_Card.setBounds(180, 0, 533, 339);
		panelPrincipal.add(panel_Card);
		panel_Card.setLayout(new CardLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		panel_Card.add(scrollPane, "name_59002504711516");
		
		tablaPrincipal = new JTable();
		tablaPrincipal.setModel(new DefaultTableModel(new Object [] [] {},
				new String [] {"Dni", "nombre y apellido", "Telefono", "Direccion", "Email", "Localidad"}));
		scrollPane.setViewportView(tablaPrincipal);
		
		JPanel panel_Reportes = new JPanel();
		panel_Card.add(panel_Reportes, "name_59012901077732");
		
		JPanel panel_Usuarios = new JPanel();
		panel_Card.add(panel_Usuarios, "name_59017934951501");
		
		JPanel panel_Gestion = new JPanel();
		panel_Card.add(panel_Gestion, "name_59021962271778");
		//panelPrincipal.add(tablePrincipal);
		
		
	}
}
